 
/*
--Project : Student Functionality Requirement Students,
*/

--PART A
USE Master;
GO
DROP DATABASE IF EXISTS schooldb;
GO
CREATE DATABASE schooldb;
GO
USE schooldb;
PRINT 'PART A Completed'

-- ****************************
-- PART B
-- ****************************
IF OBJECT_ID ( 'usp_dropTables', 'P' ) IS NOT NULL
DROP PROCEDURE usp_dropTables;  
GO       
CREATE PROCEDURE usp_dropTables
AS
BEGIN
    DROP TABLE IF EXISTS Student_Courses;
    DROP TABLE IF EXISTS CourseList;
    DROP TABLE IF EXISTS StudentContacts;
    DROP TABLE IF EXISTS ContactType;
    DROP TABLE IF EXISTS Employees;
    DROP TABLE IF EXISTS EmpJobPosition;    
    DROP TABLE IF EXISTS StudentInformation;    
END ;
GO

EXEC usp_dropTables;

PRINT 'Part B Completed'
GO


--PART C (Creating table)

CREATE TABLE StudentInformation 
(
 StudentID int NOT NULL IDENTITY(100, 1),
 Title varchar(50),
 FirstName varchar(50),
 LastName varchar(50),
 Address1 varchar(50),
 Address2 varchar(50),
 City varchar(50),
 County varchar(50),
 Zip varchar(50),
 Country varchar(50), 
 Telephone varchar(50),
 Email varchar(50),
 Enrolled varchar(50),
 AltTelephone varchar(50),
 CONSTRAINT PK_StudentID PRIMARY KEY (StudentID)
);
GO

CREATE TABLE CourseList 
(
 CourseID int NOT NULL IDENTITY(10, 1),
 CourseDescription varchar(50),
 CourseCost money,
 CourseDurationYears int,
 Notes text
 CONSTRAINT pk_CourseID PRIMARY KEY (CourseID)
);
GO

CREATE TABLE EmpJobPosition
(
 EmpJobPositionID int IDENTITY(1,1),
 EmployeePosition varchar(50)
 CONSTRAINT pk_EmpJobPositionID PRIMARY KEY (EmpJobPositionID)
);
GO

CREATE TABLE Employees 
(
 EmployeeID int IDENTITY(1000, 1), 
 EmployeeName varchar(50), 
 EmpJobPositionID int FOREIGN KEY REFERENCES EmpJobPosition(EmpJobPositionID),
 CONSTRAINT PK_EmployeeID PRIMARY KEY (EmployeeID)
);
GO


CREATE TABLE StudentContacts 
(
 ContactID int NOT NULL IDENTITY(10000, 1),
 StudentID int FOREIGN KEY REFERENCES StudentInformation (StudentID),
 ContactTypeID int,
 ContactDate date,
 EmployeeID int FOREIGN KEY REFERENCES Employees (EmployeeID),
 ContactDetails text,
 CONSTRAINT PK_ContactID PRIMARY KEY (ContactID)
);
GO

CREATE TABLE Student_Courses 
(
 StudentCourseID int IDENTITY(1,1),
 StudentID int FOREIGN KEY REFERENCES StudentInformation (StudentID),
 CourseID int FOREIGN KEY REFERENCES CourseList (CourseID),
 CourseStartDate date,
 CourseComplete date
 CONSTRAINT StudentCourseID PRIMARY KEY (StudentCourseID)
);
GO

CREATE TABLE ContactType (
 ContactTypeID int IDENTITY(1,1),
 ContactType varchar(50)
 CONSTRAINT pk_ContactTypeID PRIMARY KEY (ContactTypeID)
);

PRINT 'Part C Completed'

--PART D (Adding column, constraints and indexes)
ALTER TABLE Student_Courses
ALTER COLUMN StudentID int NOT NULL
GO

ALTER TABLE Student_Courses
ALTER COLUMN COurseID int NOT NULL
GO

ALTER TABLE Student_Courses
ADD CONSTRAINT CompKey_StudentID_CourseID Unique (StudentID, CourseID)
GO

ALTER TABLE StudentInformation
ADD CreatedDateTime datetime NOT NULL default (GETDATE())
GO

ALTER TABLE StudentInformation
DROP COLUMN AltTelephone
GO

CREATE INDEX IX_LastName
ON StudentInformation (LastName)
GO

PRINT 'Part D Completed'
GO

--*********************
--PART E
--*********************
CREATE TRIGGER trg_assignEmail
ON StudentInformation
AFTER INSERT
AS
BEGIN
 IF(SELECT Email FROM INSERTED) IS NULL
  BEGIN
   SET NOCOUNT ON;
   UPDATE StudentInformation
   SET Email = CONCAT(i.FirstName,'.',i.LastName,'@disney.com')
   FROM Inserted i
   WHERE StudentInformation.Email is NULL
  END
END
GO
PRINT 'Part E Completed'
GO


-- ****************************
--PART F A(DATA POPULATION)

INSERT INTO StudentInformation
   (FirstName,LastName)
VALUES
   ('Mickey', 'Mouse');

INSERT INTO StudentInformation
   (FirstName,LastName)
VALUES
   ('Minnie', 'Mouse');

INSERT INTO StudentInformation
   (FirstName,LastName)
VALUES
   ('Donald', 'Duck');
SELECT * FROM StudentInformation;

INSERT INTO CourseList
   (CourseDescription)
VALUES
   ('Advanced Math');

INSERT INTO CourseList
   (CourseDescription)
VALUES
   ('Intermediate Math');

INSERT INTO CourseList
   (CourseDescription)
VALUES
   ('Beginning Computer Science');

INSERT INTO CourseList
   (CourseDescription)
VALUES
   ('Advanced Computer Science');
select * from CourseList;

INSERT INTO Student_Courses
   (StudentID,CourseID,CourseStartDate)
VALUES
   (100, 10, '01/05/2018');

INSERT INTO Student_Courses
   (StudentID,CourseID,CourseStartDate)
VALUES
   (101, 11, '01/05/2018');

INSERT INTO Student_Courses
   (StudentID,CourseID,CourseStartDate)
VALUES
   (102, 11, '01/05/2018');
INSERT INTO Student_Courses
   (StudentID,CourseID,CourseStartDate)
VALUES
   (100, 11, '01/05/2018');

INSERT INTO Student_Courses
   (StudentID,CourseID,CourseStartDate)
VALUES
   (102, 13, '01/05/2018');
select * from Student_Courses;

INSERT INTO EmpJobPosition
   (EmployeePosition)
VALUES
   ('Math Instructor');

INSERT INTO EmpJobPosition
   (EmployeePosition)
VALUES
   ('Computer Science');
select * from EmpJobPosition

INSERT INTO Employees
   (EmployeeName,EmpJobPositionID)
VALUES
   ('Walt Disney', 1);

INSERT INTO Employees
   (EmployeeName,EmpJobPositionID)
VALUES
   ('John Lasseter', 2);

INSERT INTO Employees
   (EmployeeName,EmpJobPositionID)
VALUES
   ('Danny Hillis', 2);
select * from Employees;

INSERT INTO ContactType
   (ContactType)
VALUES
   ('Tutor');

INSERT INTO ContactType
   (ContactType)
VALUES
   ('Homework Support');

INSERT INTO ContactType
   (ContactType)
VALUES
   ('Conference');
SELECT * from ContactType;

INSERT INTO StudentContacts
   (StudentID,ContactTypeID,EmployeeID,ContactDate,ContactDetails)
VALUES
   (100, 1, 1000, '11/15/2017', 'Micky and Walt Math Tutoring');

INSERT INTO StudentContacts
   (StudentID,ContactTypeID,EmployeeID,ContactDate,ContactDetails)
VALUES
   (101, 2, 1001, '11/18/2017', 'Minnie and John Homework support');

INSERT INTO StudentContacts
   (StudentID,ContactTypeID,EmployeeID,ContactDate,ContactDetails)
VALUES
   (100, 3, 1001, '11/18/2017', 'Micky and Walt Conference');

INSERT INTO StudentContacts
   (StudentID,ContactTypeID,EmployeeID,ContactDate,ContactDetails)
VALUES
   (102, 2, 1002, '11/20/2017', 'Donald and Danny Homework support');

SELECT * from StudentContacts;

-- Note for Part E, use these two inserts as examples to test the trigger
-- They will also be needed if you�re using the examples for Part G
INSERT INTO StudentInformation
   (FirstName,LastName,Email)
VALUES
   ('Porky', 'Pig', 'porky.pig@warnerbros.com');
INSERT INTO StudentInformation
   (FirstName,LastName)
VALUES
   ('Snow', 'White');

PRINT 'Part F Completed'
GO

--PART G

IF OBJECT_ID ( 'usp_addQuickContacts', 'P' ) IS NOT NULL
DROP PROCEDURE usp_addQuickContacts;  
GO  
     
CREATE PROCEDURE usp_addQuickContact
@StudentEmail varchar(50),
@EmployeeName varchar(50),
@ContactDetails varchar(50),
@ContactType varchar(50)

AS
BEGIN
 DECLARE @StudentID int, @ContactTypeID int, @EmployeeID int

 SET NOCOUNT ON;
 IF NOT EXISTS (SELECT * FROM ContactType WHERE ContactType = @ContactType)
 BEGIN
  INSERT INTO ContactType
   (ContactType)
  VALUES
   (@ContactType)  
 END


 IF EXISTS (SELECT * FROM ContactType WHERE ContactType = @ContactType)
 BEGIN
  SELECT @StudentID = StudentID FROM StudentInformation WHERE Email = @StudentEmail
  SELECT @ContactTypeID = ContactTypeID FROM ContactType WHERE ContactType = @ContactType
  SELECT @EmployeeID = EmployeeID FROM Employees WHERE EmployeeName = @EmployeeName

  INSERT INTO StudentContacts 
   (StudentID, ContactTypeID, ContactDate, EmployeeID, ContactDetails)
  VALUES
   (@StudentID, @ContactTypeID, (GETDATE()), @EmployeeID, @ContactDetails);  
 END
END
GO

EXEC usp_addQuickContact 'minnie.mouse@disney.com','John Lasseter','Minnie getting Homework Support from John','Homework Support'
EXEC usp_addQuickContact 'porky.pig@warnerbros.com','John Lasseter','Porky studying with John for Test prep','Test Prep'

PRINT 'Part G Completed'


--Part H (creating & execute usp_getCourseRosterByName)
IF OBJECT_ID ( 'usp_getCourseRosterByName', 'P' ) IS NOT NULL
DROP PROCEDURE usp_getCourseRosterByName;  
GO  
CREATE PROCEDURE usp_getCourseRosterByName
@CourseDescription varchar(50)
AS
 BEGIN 
  SELECT cl.CourseDescription, si.FirstName, si.LastName
  FROM CourseList cl
   INNER JOIN Student_Courses sc ON sc.CourseID = cl.CourseID
   INNER JOIN StudentInformation si ON sc.StudentID = si.StudentID
  WHERE CourseDescription = @CourseDescription

END
GO

EXEC usp_getCourseRosterByName @CourseDescription = 'Intermediate Math';

PRINT 'Part H Completed'


--Part I (Create & select from vtutorContacts View)  = ERROR (SELECT >
IF OBJECT_ID ( 'vtutorContacts', 'V' ) IS NOT NULL
DROP VIEW vtutorContacts;  
GO  
CREATE VIEW vtutorContacts
AS
 SELECT e.EmployeeName, si.FirstName+si.LastName AS StudentName, sc.ContactDetails, sc.ContactDate
 FROM Employees e INNER JOIN StudentContacts sc on e.EmployeeID = sc.EmployeeID
 INNER JOIN StudentInformation si on sc.StudentID = si.StudentID
 INNER JOIN ContactType ct on sc.ContactTypeID = ct.ContactTypeID
 where ct.ContactType='Tutor'
 Go

SELECT *
FROM vtutorContacts


 /*
 Project : Student Functionality Requirement Students,


Topic : Courses and Tutors

The following data model is designed to hold information relating to Students, Student Courses and Instructors who tutor students.


For this scenario, we need to define the following entities:


    Student Information

    Courses

    Student Courses (enrollment)

    Employees (instructors)

    Student Contacts (Contact between the Student and the Instructor)

    Contact Types (Tutor, Test support,etc..)


The entities are based on the ER diagram below and use the following rules to determine the table relationships.


    A Student can enroll in one or many Courses

    A Course can have one or many Students enrolled in it.

    A Student can have zero, one, or many forms of contact with the Course Tutor

    An Employee (Tutor) can have many contacts

    A contact Type (Tutor, Test support,etc..) can have zero, one, or many contacts


The design allows ~

    a Student to enroll in one or multiple Courses,

    a Course allowing one or more Students enrolled in it.

    a student may be in contact with the Course Tutor many times using many different forms of contact.

    an instructor can connect with many contacts involving many Students



Entity Relationship Diagram (ERD)


Setting up the project


    Make a copy of the project.sql template file (also linked in Canvas) to help guide you through the project.

        Download it as a text file and work on it locally (can still have the .sql extension)

        In Google Drive -> File->Download As -> Text File

    Read through the A-I part (sections) below, and add your responses to the problems in your local copy of the project.sql file.

    Ensure the ENTIRE project.sql runs without errors (use sql commenting if there are any problems you are unable to finish)

    Upload your project.sql text file through Canvas in the Final Project Assignment.



Notes on project.sql :


Each part has some documentation(below and in the project.sql template) to describe the specific statements needed to answer each part. While the execution order of the script should remain sequential (e.g. Part C executes before B, which executes before A), the order in which you work on the script can happen in any order you want (e.g. if you want to start with part G, and it doesn�t depend on something earlier in the script, go for it).


Also, the HINTS with test data are merely �examples�, and are NOT REQUIRED in your response. They are there to help guide you. I�ll be looking at how you constructed your logic for each of the Parts below instead of resulting data from each Part�s query execution.



The total project is worth 280 points
Rubric:
    Part A & Part F are supplied 0 points
    No errors when executing the entire script - 25 points
    Part B - 40 points
    Part C - 60 points
    Part D - 25 points
    Part E - 40 points
    Part G - 40 points
    Part H - 25 points
    Part I - 25 points


Part Task Descriptions

    Part A - Creating the database

        Use the provided template, no action required.


    Part B -Define and execute usp_dropTables

        Create a Stored Procedure : usp_dropTables, which executes a series of DROP TABLE statements to remove all the tables created (from the ERD). To prevent errors on trying to drop a table that may not exist, use DROP TABLE IF EXISTS version of your statements.

        HINT: Looking at the ERD, CONSTRAINTS are implied.( trying to drop a table that is a FK to another table will fail). The order in which you drop the tables is important. When running the stored procedure and the script multiple times, it should should run without errors.


        HINT: test with EXEC usp_dropTables;


    Part C - Define and create the tables from the ERD

        Write the CREATE TABLE statements for each of the tables in the ERD.

        Integrate the PRIMARY KEY and FOREIGN KEY CONSTRAINTS in the CREATE TABLE statement itself.

            Note: We didn't cover this, but here's a reference to the statement format

            https://docs.microsoft.com/en-us/sql/t-sql/statements/create-table-transact-sql or try Google for examples on specifying the PRIMARY and FOREIGN KEY CONSTRAINTS when the table is created.

        General notes about Table and ERD

            Many of the fields accept NULL, review the INSERT statements in PART F to determine the �NOT NULL� fields, as well as the implied field types.

            For alpha-numeric data, use char() datatype.

                Refer to the test examples and the INSERT statements (in Part F) to determine the length of each field (e.g. script should execute without the need to truncate data)

            Use the following int IDENTITIES for the relevant PRIMARY KEYS in each of the Tables. Again, refer to PART F to help guide how the columns are declared.

                StudentInformation. StudentID - starts at 100, increments 1

                CourseList.CourseID - starts at 10, increments 1

                Employees.EmployeeID - starts at 1000, increments 1

                Contacts.ContactID -starts at 10000, increments 1

                StudentCourseID, EmpJobPositionID, ContactTypeID all start at 1, increments 1


    Part D - Adding columns, constraints and indexes

        Modify the table structures and constraints for the following scenarios:

            Prevent duplicate records in Student_Courses.

                Specifically, consider a duplicate as a matching of both StudentIDs and CourseIDs (e.g. Composite Key needs to be unique)

            Add a new column to the StudentInformation table called CreatedDateTime. It should default to the current timestamp when a record is added to the table.

            Remove the AltTelephone column from the StudentInformation table

            Add an Index called IX_LastName on the StudentInformation table.


    Part E - Create and apply a Trigger called trg_assignEmail

        Create a trigger on the StudentInfomation table : trg_assignEmail

            When a new row is inserted into StudentInformation without the Email field specified, the trigger will fire and will automatically update the Email field of the record. The Email field will be automatically constructed using the following pattern firstName.lastName@disney.com (e.g. Erik Kellener would be Erik.Kellener@disney.com)

            If the insert statement already contains an email address, the trigger does not update the Email field (e.g. ignores the trigger�s action)

        HINT: Use the following test cases

            Case #1 Test when the email is specified.

                INSERT INTO StudentInformation (FirstName,LastName,Email) VALUES ('Porky', 'Pig','porky.pig@warnerbros.com');

            Case #2 Test when the email address is not specified.

                INSERT INTO StudentInformation (FirstName,LastName) VALUES ('Snow', 'White');


    Part F - Populating sample data

        Use the template, no action required.



    Part G - Create and execute usp_addQuickContacts

        Create a stored procedure that allows for quick adds of Student and Instructor contacts: usp_addQuickContacts.

        The procedure will accept 4 parameters:

            Student Email

            EmployeeName

            contactDetails

            contactType

        And performs an INSERT into the StudentsContacts table.

        When inserting into StudentsContacts, the ContactDate field will automatically default to the current Date.

        Additionally, upon calling the usp_addQuickContacts procedure,

            If the contactType parameter value doesn�t already exist in the ContactType table, it's first inserted as an additional contactType (e.g. append a new record) AND then used with an INSERT statement to the StudentContacts.

            If the contactType parameter value does already exist in ContactType, it's corresponding ID is added as part of the StudentsContacts INSERT statement.

        Note: Assume parameters passed to the procedure are valid (e.g. all Student email addresses, and Employee names are correctly entered passed to the procedure)

        HINT: You'll want to initially establish the contactTypeID before moving onto the INSERT statement.

        HINT:Use these test cases to verify the desired output (Note: Make sure the trg_assignEmail is created and applied before running these test cases)

            EXEC usp_addQuickContacts 'minnie.mouse@disney.com','John Lasseter','Minnie getting Homework Support from John','Homework Support'

            EXEC usp_addQuickContacts 'porky.pig@warnerbros.com','John Lasseter','Porky studying with John for Test prep','Test Prep'


    Part H - Create and execute usp_getCourseRosterByName

        Create a stored procedure: usp_getCourseRosterByName.

        It takes 1 parameter, CourseDescription and returns the list of the student�s FirstName, and LastName along with the CourseDescription they are enrolled in. (E.g. Student_Courses and CourseList tables are used)

        Note: Use JOINS. Do not use multiple query statements AND subqueries in the procedure to form your answer..

        HINT : Use this as a test example:

            EXEC usp_getCourseRosterByName 'Intermediate Math';

            Expected results:

                Intermediate Math Mickey Mouse

                Intermediate Math Minnie Mouse

                Intermediate Math Donald Duck



    Part I Create and Select from vtutorContacts View

        Create a view : vtutorContacts, which returns the results from StudentContacts displaying fields EmployeeName, StudentName, ContactDetails, and ContactDate where the contactType is �Tutor�.

            EmployeeName doesn�t exist in StudentContacts, and may require a JOIN from another table.

            StudentName doesn't exist, but should be in the form FirstName+' '+LastName. Ensuring both First and Last name are properly trimmed.

*/