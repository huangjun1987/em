﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ZooProject
{
    public partial class ListAnimals : Form
    {
        private PrintPreviewDialog printPreviewDialog1 = new PrintPreviewDialog();
        private PrintDocument printDocument1 = new PrintDocument();

        // Declare a string to hold the entire document contents.
        private string documentContents;

        // Declare a variable to hold the portion of the document that
        // is not printed.
        private string stringToPrint;

        public ListAnimals()
        {
            InitializeComponent();
            DisplayAnimals();
            printDocument1.PrintPage +=
                new PrintPageEventHandler(printDocument1_PrintPage);

        }

  

        private ZooRepository zooRepository = new ZooRepository();
        private void DisplayAnimals()
        {
            listViewAnimals.Items.Clear();
            listViewAnimals.View = System.Windows.Forms.View.Details;
            listViewAnimals.Columns.Add("Name", 100);
            listViewAnimals.Columns.Add("CategoryName", 100);
            listViewAnimals.Columns.Add("Quantity", 100);

            var animals = zooRepository.GetAnimals();

            if (animals.Count > 0)
            {
                for (int i = 0; i < animals.Count; i++)
                {

                    var animal = animals[i];
                    listViewAnimals.Items.Add(animal.Name);
                    listViewAnimals.Items[i].SubItems.Add(zooRepository.GetCategoryNameByCategoryId(animal.CategoryId));
                    listViewAnimals.Items[i].SubItems.Add(animal.Quantity.ToString());
                }

            }
        }

        private void addButton_Click(object sender, EventArgs e)
        {
            this.Hide();
            AddAnimal addAnimal = new AddAnimal();
            addAnimal.Show();
        }

        private void listViewAnimals_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void editButton_Click(object sender, EventArgs e)
        {
            if (listViewAnimals.SelectedItems.Count < 1)
            {
                MessageBox.Show("Please select an animal!");
                return;
            }
            var selectedRow = listViewAnimals.SelectedItems[0];
            this.Hide();
            EditAnimal editAnimal = new EditAnimal(selectedRow.Text);
            editAnimal.Show();

        }

        private void exitButton_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void deleteButton_Click(object sender, EventArgs e)
        {
            if (listViewAnimals.SelectedItems.Count < 1)
            {
                MessageBox.Show("Please select an animal!");
                return;
            }
            var selectedRow = listViewAnimals.SelectedItems[0];
            zooRepository.DeleteAnimal(selectedRow.Text, DelegateHelper.DisplayMessage);
            DisplayAnimals();

        }

        private void ReadDocument()
        {
            string docName = "listAnimals.pdf";
            printDocument1.DocumentName = docName;
            documentContents = GetStringToPrint();
            stringToPrint = documentContents;
        }

        private string GetStringToPrint()
        {
            var sb = new StringBuilder();
            sb.Append("List of Animals in the Zoo:\n");
            foreach (var animal in zooRepository.GetAnimals())
            {
                sb.Append($"Name: {animal.Name}, Category: {animal.AnimalCategory.CategoryName}," +
                    $" Name: {animal.Name}, Quantity: {animal.Quantity}\n");
            }
            return sb.ToString();
        }

        void printDocument1_PrintPage(object sender, PrintPageEventArgs e)
        {
            // Sets the value of charactersOnPage to the number of characters 
            // of stringToPrint that will fit within the bounds of the page.
            e.Graphics.MeasureString(stringToPrint, this.Font,
                e.MarginBounds.Size, StringFormat.GenericTypographic,
                out int charactersOnPage, out int linesPerPage);

            // Draws the string within the bounds of the page.
            e.Graphics.DrawString(stringToPrint, this.Font, Brushes.Black,
            e.MarginBounds, StringFormat.GenericTypographic);

            // Remove the portion of the string that has been printed.
            stringToPrint = stringToPrint.Substring(charactersOnPage);

            // Check to see if more pages are to be printed.
            e.HasMorePages = (stringToPrint.Length > 0);

            // If there are no more pages, reset the string to be printed.
            if (!e.HasMorePages)
                stringToPrint = documentContents;
        }



        private void buttonPrint_Click(object sender, EventArgs e)
        {
            ReadDocument();
            printPreviewDialog1.Document = printDocument1;
            printPreviewDialog1.ShowDialog();


        }
    }
}
