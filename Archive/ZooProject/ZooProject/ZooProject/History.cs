﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ZooProject
{
    public partial class History : Form
    {
        private ZooRepository zooRepository = new ZooRepository();



        public History()
        {
            InitializeComponent();
            var animals = zooRepository.GetAnimals().ToArray();

            for (int i = 0; i < animals.Length; i++)
            {
                treeViewHistory.Nodes.Add(animals[i].Name);
                var history = zooRepository.GetHistoryByAnimalId(animals[i].Id);
                foreach (var entry in history)
                {
                    treeViewHistory.Nodes[i].Nodes.Add($"EntryDate: {entry.EntryDate}, " +
                        $"EntryType: {entry.EntryType}, AnimalName: {entry.AnimalName}," +
                        $" CategoryName: {entry.CategoryName}, Quantity: {entry.Quantity}");
                }
            }


        }
    }
}
