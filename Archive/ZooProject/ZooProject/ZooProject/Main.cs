﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ZooProject
{
    public partial class Main : Form
    {
        public Main()
        {
            InitializeComponent();
            IsMdiContainer = true;



        }

        private void viewHistoryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var form = new History();
            form.MdiParent = this;
            form.Show();
        }

        private void addAnimalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var form = new AddAnimal();
            form.MdiParent = this;
            form.Show();
            


        }

        private void listAnimalsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var form = new ListAnimals();
            form.MdiParent = this;
            form.Show();


        }
    }
}
