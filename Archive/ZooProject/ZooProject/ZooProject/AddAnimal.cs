﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace ZooProject
{
    public partial class AddAnimal : ZooProject.ParentForm
    {
        private ZooRepository zooRepository = new ZooRepository();

        private void LoadCategories()
        {
            listBoxCategory.Items.Clear();
            var categories = zooRepository.GetCategories();
            listBoxCategory.DataSource = categories;
            listBoxCategory.DisplayMember = "CategoryName";
            listBoxCategory.ValueMember = "Id";
        }
        public AddAnimal()
        {
            InitializeComponent();
            LoadCategories();

        }
        private void buttonOK_Click(object sender, EventArgs e)
        {
            var isSuccess = zooRepository.AddAnimal(textBoxName.Text, listBoxCategory.SelectedValue.ToString(), (int)numericUpDownQuantity.Value, DelegateHelper.DisplayMessage);

            if (isSuccess)
            {
                RedirectToListForms();
            }

        }

     
    }
}
