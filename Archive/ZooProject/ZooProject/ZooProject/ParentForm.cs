﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ZooProject
{
    public partial class ParentForm : Form
    {
        public ParentForm()
        {
            InitializeComponent();
        }
        public void RedirectToListForms()

        {

            this.Hide();

            ListAnimals form = new ListAnimals();

            form.Show();

        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            RedirectToListForms();
        }
    }
}
