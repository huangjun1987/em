﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace ZooProject
{
    public partial class EditAnimal : ZooProject.ParentForm
    {
        private ZooRepository zooRepository = new ZooRepository();
        private Animal animal;
        private List<AnimalCategory> categories;

        public EditAnimal(string animalName)
        {
            InitializeComponent();
            categories = zooRepository.GetCategories();
            LoadCategories();
            animal = zooRepository.GetAnimalByName(animalName);
            LoadAnimal();


        }
        private void LoadCategories()
        {
            listBoxCategory.Items.Clear();
            listBoxCategory.DataSource = categories;
            listBoxCategory.DisplayMember = "CategoryName";
            listBoxCategory.ValueMember = "Id";
        }


        private void LoadAnimal()
        {
            textBoxName.Text = animal.Name;
            numericUpDownQuantity.Value = animal.Quantity;
            UpdateSelectedCategory(categories);
        }

        private void UpdateSelectedCategory(List<AnimalCategory> categories)
        {
            var categoriesArray = categories.ToArray();
            for (int i = 0; i < categoriesArray.Length; i++)
            {
                if (animal.CategoryId == categoriesArray[i].Id)
                {
                    listBoxCategory.SelectedIndex = i;
                    return;
                }
            }
        }


        private void buttonOk_Click(object sender, EventArgs e)
        {
            var isSuccess = zooRepository.EditAnimal(animal.Id, textBoxName.Text, 
                listBoxCategory.SelectedValue.ToString(), 
                (int)numericUpDownQuantity.Value, DelegateHelper.DisplayMessage);

            if (isSuccess)
            {
                RedirectToListForms();
            }


        }
    }
}
