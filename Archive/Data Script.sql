USE [Zoo]
GO

/****** Object:  Table [dbo].[AnimalCategory]    Script Date: 2/25/2020 4:08:57 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[AnimalCategory](
	[Id] [int] NOT NULL,
	[CategoryName] [varchar](50) NOT NULL,
 CONSTRAINT [PK_AnimalCategory] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

INSERT INTO [dbo].[AnimalCategory]
           ([Id]
           ,[CategoryName])
     VALUES
           (1
           ,'Birds')
GO

INSERT INTO [dbo].[AnimalCategory]
           ([Id]
           ,[CategoryName])
     VALUES
           (2
           ,'Amphibians')
GO

INSERT INTO [dbo].[AnimalCategory]
           ([Id]
           ,[CategoryName])
     VALUES
           (3
           ,'Invertebrate')
GO

INSERT INTO [dbo].[AnimalCategory]
           ([Id]
           ,[CategoryName])
     VALUES
           (4
           ,'Reptiles')
GO

INSERT INTO [dbo].[AnimalCategory]
           ([Id]
           ,[CategoryName])
     VALUES
           (5
           ,'Mammals')
GO

INSERT INTO [dbo].[AnimalCategory]
           ([Id]
           ,[CategoryName])
     VALUES
           (6
           ,'Fish')
GO

USE [Zoo]
GO

/****** Object:  Table [dbo].[Animal]    Script Date: 2/25/2020 4:19:50 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

USE [Zoo]
GO

/****** Object:  Table [dbo].[Animal]    Script Date: 2/25/2020 4:21:45 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Animal](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CategoryId] [int] NOT NULL,
	[Name] [varchar](100) NOT NULL,
	[Quantity] [int] NOT NULL,
 CONSTRAINT [PK_Animal] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[Animal]  WITH CHECK ADD  CONSTRAINT [FK_Animal_AnimalCategory] FOREIGN KEY([CategoryId])
REFERENCES [dbo].[AnimalCategory] ([Id])
GO

ALTER TABLE [dbo].[Animal] CHECK CONSTRAINT [FK_Animal_AnimalCategory]
GO

INSERT INTO [dbo].[Animal]
           ([CategoryId]
           ,[Name]
           ,[Quantity])
     VALUES
           (1
           ,'Eagle'
           ,10)
GO

INSERT INTO [dbo].[Animal]
           ([CategoryId]
           ,[Name]
           ,[Quantity])
     VALUES
           (1
           ,'Sparrow'
           ,2)
GO

INSERT INTO [dbo].[Animal]
           ([CategoryId]
           ,[Name]
           ,[Quantity])
     VALUES
           (2
           ,'Bullfrog'
           ,20)
GO
INSERT INTO [dbo].[Animal]
           ([CategoryId]
           ,[Name]
           ,[Quantity])
     VALUES
           (2
           ,'Salamander'
           ,30)
GO

INSERT INTO [dbo].[Animal]
           ([CategoryId]
           ,[Name]
           ,[Quantity])
     VALUES
           (3
           ,'Spider'
           ,40)
GO

INSERT INTO [dbo].[Animal]
           ([CategoryId]
           ,[Name]
           ,[Quantity])
     VALUES
           (4
           ,'Lizard'
           ,40)
GO

INSERT INTO [dbo].[Animal]
           ([CategoryId]
           ,[Name]
           ,[Quantity])
     VALUES
           (5
           ,'Elephant'
           ,40)
GO

INSERT INTO [dbo].[Animal]
           ([CategoryId]
           ,[Name]
           ,[Quantity])
     VALUES
           (6
           ,'Carp'
           ,40)
GO

USE [Zoo]
GO

/****** Object:  Table [dbo].[AnimalHistory]    Script Date: 2/29/2020 10:24:41 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[AnimalHistory](
	[AnimalId] [int] NOT NULL,
	[EntryDate] [datetime] NOT NULL,
	[EntryType] [varchar](50) NOT NULL,
	[AnimalName] [varchar](100) NOT NULL,
	[CategoryName] [varchar](50) NOT NULL,
	[Quantity] [int] NOT NULL,
 CONSTRAINT [PK_AnimalHistory] PRIMARY KEY CLUSTERED 
(
	[AnimalId] ASC,
	[EntryDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[AnimalHistory]  WITH CHECK ADD  CONSTRAINT [FK_AnimalHistory_Animal] FOREIGN KEY([AnimalId])
REFERENCES [dbo].[Animal] ([Id])
GO

ALTER TABLE [dbo].[AnimalHistory] CHECK CONSTRAINT [FK_AnimalHistory_Animal]
GO


INSERT INTO [dbo].[AnimalHistory]
           ([AnimalId]
           ,[EntryDate]
           ,[EntryType]
		   ,[AnimalName]
           ,[CategoryName]
           ,[Quantity])
     VALUES
           (1
           ,'01/01/2020'
           ,'Create'
		   ,'Eagle'
           ,'Birds'
           ,10)
GO

INSERT INTO [dbo].[AnimalHistory]
           ([AnimalId]
           ,[EntryDate]
           ,[EntryType]
		   ,[AnimalName]
           ,[CategoryName]
           ,[Quantity])
     VALUES
           (2
           ,'02/01/2020'
           ,'Create'
		   ,'Sparrow'
           ,'Birds'
           ,2)
GO
INSERT INTO [dbo].[AnimalHistory]
           ([AnimalId]
           ,[EntryDate]
           ,[EntryType]
		   ,[AnimalName]
           ,[CategoryName]
           ,[Quantity])
     VALUES
           (3
           ,'03/01/2020'
           ,'Create'
		   ,'Bullfrog'
           ,'Amphibians'
           ,20)
GO
INSERT INTO [dbo].[AnimalHistory]
           ([AnimalId]
           ,[EntryDate]
           ,[EntryType]
		   ,[AnimalName]
           ,[CategoryName]
           ,[Quantity])
     VALUES
           (4
           ,'04/01/2020'
           ,'Create'
		   ,'Salamander'
           ,'Amphibians'
           ,30)
GO
INSERT INTO [dbo].[AnimalHistory]
           ([AnimalId]
           ,[EntryDate]
           ,[EntryType]
		   ,[AnimalName]
           ,[CategoryName]
           ,[Quantity])
     VALUES
           (5
           ,'05/01/2020'
           ,'Create'
		   ,'Spider'
           ,'Invertebrate'
           ,40)
GO
INSERT INTO [dbo].[AnimalHistory]
           ([AnimalId]
           ,[EntryDate]
           ,[EntryType]
		   ,[AnimalName]
           ,[CategoryName]
           ,[Quantity])
     VALUES
           (6
           ,'06/01/2020'
           ,'Create'
		   ,'Lizard'
           ,'Reptiles'
           ,40)
GO
INSERT INTO [dbo].[AnimalHistory]
           ([AnimalId]
           ,[EntryDate]
           ,[EntryType]
		   ,[AnimalName]
           ,[CategoryName]
           ,[Quantity])
     VALUES
           (7
           ,'06/01/2020'
           ,'Create'
		   ,'Elephant'
           ,'Mammals'
           ,40)
GO
INSERT INTO [dbo].[AnimalHistory]
           ([AnimalId]
           ,[EntryDate]
           ,[EntryType]
		   ,[AnimalName]
           ,[CategoryName]
           ,[Quantity])
     VALUES
           (8
           ,'07/01/2020'
           ,'Create'
		   ,'Carp'
           ,'Fish'
           ,40)
GO
