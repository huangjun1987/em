﻿using System;
using System.Windows.Forms;

namespace ZooApp
{
    public partial class Main : Form
    {
        public Main()
        {
            InitializeComponent();
            IsMdiContainer = true;
        }

        private void viewAnimalsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var form = new ListAnimals();
            form.MdiParent = this;
            form.Show();
        }

        private void addAnimalToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            var form = new AddAnimalInherited();
            form.MdiParent = this;
            form.Show();
        }

        private void viewHistoryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var form = new History();
            form.MdiParent = this;
            form.Show();
        }
    }
}
