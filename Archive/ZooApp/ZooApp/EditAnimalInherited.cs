﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace ZooApp
{
    public partial class EditAnimalInherited : ZooApp.ParentForm
    {
        private ZooRepository zooRepository = new ZooRepository();
        private Animal animal;
        private List<AnimalCategory> categories;
        public EditAnimalInherited(string animalName)
        {
            InitializeComponent();
            categories = zooRepository.GetCategories();
            LoadCategories();
            animal = zooRepository.GetAnimalByName(animalName);
            LoadAnimal();
        }
        private void LoadCategories()
        {
            listBoxCategory.Items.Clear();
            
            listBoxCategory.DataSource = categories;
            listBoxCategory.DisplayMember = "CategoryName";
            listBoxCategory.ValueMember = "Id";
        }

        private void UpdateSelectedCategory(List<AnimalCategory> categories)
        {
            var categoriesArray = categories.ToArray();
            for (int i = 0; i < categoriesArray.Length; i++)
            {
                if (animal.CategoryId == categoriesArray[i].Id)
                {
                    listBoxCategory.SelectedIndex = i;
                    return;
                }
            }
        }

        private void LoadAnimal()
        {
            textBoxName.Text = animal.Name;
            numericUpDownQuantity.Value = animal.Quantity;
            UpdateSelectedCategory(categories);
        }

        private void buttonOK_Click(object sender, EventArgs e)
        {
            var isSuccess = zooRepository.EditAnimal(animal.Id, textBoxName.Text, listBoxCategory.SelectedValue.ToString(), (int)numericUpDownQuantity.Value, DelegateHelper.DisplayMessage);

            if (isSuccess)
            {
                RedirectToListForms();
            }
        }
    }
}
