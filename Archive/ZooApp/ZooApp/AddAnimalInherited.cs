﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace ZooApp
{
    public partial class AddAnimalInherited : ZooApp.ParentForm
    {
        private ZooRepository zooRepository = new ZooRepository();

        public AddAnimalInherited()
        {
            InitializeComponent();
            LoadCategories();
        }

        private void LoadCategories()
        {
            listBoxCategory.Items.Clear();
            var categories = zooRepository.GetCategories();
            listBoxCategory.DataSource = categories;
            listBoxCategory.DisplayMember = "CategoryName";
            listBoxCategory.ValueMember = "Id";
        }

        private void buttonOK_Click(object sender, EventArgs e)
        {
            var isSuccess = zooRepository.AddAnimal(textBoxName.Text, listBoxCategory.SelectedValue.ToString(), (int)numericUpDownQuantity.Value, DelegateHelper.DisplayMessage);

            if (isSuccess)
            {
                RedirectToListForms();
            }
        }

        private void buttonOK_Click_1(object sender, EventArgs e)
        {
            var isSuccess = zooRepository.AddAnimal(textBoxName.Text, listBoxCategory.SelectedValue.ToString(), (int)numericUpDownQuantity.Value, DelegateHelper.DisplayMessage);

            if (isSuccess)
            {
                RedirectToListForms();
            }
        }
    }
}
