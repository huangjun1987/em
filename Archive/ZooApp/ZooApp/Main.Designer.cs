﻿namespace ZooApp
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.viewAnimalsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addAnimalToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.viewHistoryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.GripMargin = new System.Windows.Forms.Padding(2, 2, 0, 2);
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(40, 40);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.viewAnimalsToolStripMenuItem,
            this.addAnimalToolStripMenuItem1,
            this.viewHistoryToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1466, 52);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // viewAnimalsToolStripMenuItem
            // 
            this.viewAnimalsToolStripMenuItem.Name = "viewAnimalsToolStripMenuItem";
            this.viewAnimalsToolStripMenuItem.Size = new System.Drawing.Size(218, 48);
            this.viewAnimalsToolStripMenuItem.Text = "View Animals";
            this.viewAnimalsToolStripMenuItem.Click += new System.EventHandler(this.viewAnimalsToolStripMenuItem_Click);
            // 
            // addAnimalToolStripMenuItem1
            // 
            this.addAnimalToolStripMenuItem1.Name = "addAnimalToolStripMenuItem1";
            this.addAnimalToolStripMenuItem1.Size = new System.Drawing.Size(196, 48);
            this.addAnimalToolStripMenuItem1.Text = "Add Animal";
            this.addAnimalToolStripMenuItem1.Click += new System.EventHandler(this.addAnimalToolStripMenuItem1_Click);
            // 
            // viewHistoryToolStripMenuItem
            // 
            this.viewHistoryToolStripMenuItem.Name = "viewHistoryToolStripMenuItem";
            this.viewHistoryToolStripMenuItem.Size = new System.Drawing.Size(208, 48);
            this.viewHistoryToolStripMenuItem.Text = "View History";
            this.viewHistoryToolStripMenuItem.Click += new System.EventHandler(this.viewHistoryToolStripMenuItem_Click);
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(16F, 31F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1466, 609);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Main";
            this.Text = "Main";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem viewAnimalsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addAnimalToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem viewHistoryToolStripMenuItem;
    }
}