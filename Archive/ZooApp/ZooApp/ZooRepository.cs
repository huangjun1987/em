﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ZooApp
{
    public class ZooRepository
    {
        private ZooEntities zooEntities = new ZooEntities();

        internal List<Animal> GetAnimals()
        {
            return zooEntities.Animals.Include("AnimalCategory").ToList();
        }

        internal string GetCategoryNameByCategoryId(int categoryId)
        {
            return zooEntities.AnimalCategories.Where(c => c.Id == categoryId).Select(c => c.CategoryName).FirstOrDefault();
        }

        internal Animal GetAnimalByName(string animalName)
        {
            return zooEntities.Animals.Where(a => a.Name == animalName).FirstOrDefault();
        }

        internal List<AnimalHistory> GetHistoryByAnimalId(int id)
        {
            return zooEntities.AnimalHistories.Where(h => h.AnimalId == id).OrderByDescending(h => h.EntryDate).ToList();
        }

        internal List<AnimalCategory> GetCategories()
        {
            return zooEntities.AnimalCategories.ToList();
        }

        internal bool AddAnimal(string name, string categoryIdStr, int quantity, Action<string> displayMessage)
        {
            if (string.IsNullOrWhiteSpace(name))
            {
                displayMessage("Name cannot be empty");
                return false;
            }
            if (!int.TryParse(categoryIdStr, out int categoryId) || !zooEntities.AnimalCategories.Any(c => c.Id == categoryId))
            {
                displayMessage("Invalid category");
                return false;
            }

            if (quantity < 1)
            {
                displayMessage("Please enter a quantity greater than 0. ");
                return false;
            }

            var animal = new Animal()
            {
                Name = name,
                CategoryId = categoryId,
                Quantity = quantity
            };
            zooEntities.Animals.Add(animal);
            zooEntities.SaveChanges();
            var animalHistory = new AnimalHistory
            {
                AnimalId = animal.Id,
                EntryDate = DateTime.Now,
                EntryType = "Create",
                AnimalName = animal.Name,
                CategoryName = GetCategoryNameByCategoryId(animal.CategoryId),
                Quantity = animal.Quantity
            };
            zooEntities.AnimalHistories.Add(animalHistory);
            zooEntities.SaveChanges();
            displayMessage($"Successfully added animal: {name}.");
            return true;
        }

        internal bool EditAnimal(int id, string name, string categoryIdStr, int quantity, Action<string> displayMessage)
        {
            if (string.IsNullOrWhiteSpace(name))
            {
                displayMessage("Name cannot be empty");
                return false;
            }
            if (!int.TryParse(categoryIdStr, out int categoryId) || !zooEntities.AnimalCategories.Any(c => c.Id == categoryId))
            {
                displayMessage("Invalid category");
                return false;
            }

            if (quantity < 1)
            {
                displayMessage("Please enter a quantity greater than 0. ");
                return false;
            }

            var animal = zooEntities.Animals.Where(a => a.Id == id).FirstOrDefault();
            if (animal == null)
            {
                displayMessage("Couldn't find the animal you are trying to edit.");
                return false;
            }
            animal.Name = name;
            animal.Quantity = quantity;
            animal.CategoryId = categoryId;
            zooEntities.SaveChanges();
            var animalHistory = new AnimalHistory
            {
                AnimalId = animal.Id,
                EntryDate = DateTime.Now,
                EntryType = "Update",
                AnimalName = animal.Name,
                CategoryName = GetCategoryNameByCategoryId(animal.CategoryId),
                Quantity = animal.Quantity
            };
            zooEntities.AnimalHistories.Add(animalHistory);
            zooEntities.SaveChanges();
            displayMessage($"Successfully edited animal: {name}.");
            return true;
        }

        internal void DeleteAnimal(string name, Action<string> displayMessage)
        {
            var animal = GetAnimalByName(name);

            if (animal == null)
            {
                displayMessage("Couldn't find the animal you are trying to delete.");
                return;
            }
            var histories = zooEntities.AnimalHistories.Where(h => h.AnimalId == animal.Id);
            if (histories.Any())
            {
                zooEntities.AnimalHistories.RemoveRange(histories);
                zooEntities.SaveChanges();
            }          
            zooEntities.Animals.Remove(animal);
            zooEntities.SaveChanges();
            displayMessage($"Successfully deleted animal: {name}.");
        }
    }
}
